![git image](https://git-scm.com/images/logos/1color-orange-lightbg@2x.png)

# **What is git?**

**By far, the most widely used modern version control system in the world today is Git.**<br/>
**Git is an example of a DVCS (Distributed Version Control System).The main difference between centralised and distributed is the working version copy present in local repository.And it acts as a backup in the case of server down or server crash.**<br/>
**In addition to being distributed, Git has been designed with performance, security and flexibility in mind.**<br/>

![git working](https://git-scm.com/images/about/index1@2x.png)     ![version control image](https://homes.cs.washington.edu/~mernst/advice/version-control-fig3.png)

<br/><br/>

# **How does it work?**
  
  **Git stores snapshots, not differences**
  <br/><br/>

  ![git vs vcs](https://images.slideplayer.com/18/6171592/slides/slide_6.jpg)
  <br/><br/>

# **Git branch**
 
 **A branch in Git is simply a lightweight movable pointer to one of these commits. The default branch name in Git is master . As you start making commits, you're given a master branch that points to the last commit you made. Every time you commit, the master branch pointer moves forward automatically.**

  ![git branch](https://www.w3docs.com/uploads/media/default/0001/03/1e5d7590562b3b214008617211b2539ce2bddfaf.png)
   <br/><br/>

## **Git branching and merging**

 ![branching and merging](https://res.cloudinary.com/practicaldev/image/fetch/s--7lvYimJG--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://cl.ly/430Q2w473e2R/Image%25202018-04-30%2520at%25201.07.58%2520PM.png)
  <br/><br/>

# **Basic Git commands**
<br/><br/>

![git commands](https://image.slidesharecdn.com/gitworkflowinagiledevelopment-140520040805-phpapp01/95/git-workflow-in-agile-development-4-638.jpg?cb=1400559867)



