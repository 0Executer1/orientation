 ![Docker image](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)

  # **What is docker?**
  
  ![Docker demo](https://res.cloudinary.com/practicaldev/image/fetch/s--nbgRyC5P--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/0a6fv12nfe5j0tklfik1.png)

 **Docker is a software platform for building applications based on containers.**<br/>
 **Docker helps developers build lightweight and portable software containers that simplify application development, testing, and deployment.**<br/><br/>

 # **Why docker?**
   <br/>
  ![Docker effect](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/10/virtual-machine-vs-docker-example-what-is-docker-container-edureka-1.png)
  <br/><br/>

**As the above image demonstrates that docker efficiantly manages the memory of the hardware.**
 <br/><br/>

# **Docker containers, images and registers**
 <br/>
 ![docker image - image](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/container-docker-introduction/media/docker-containers-images-registries/taxonomy-of-docker-terms-and-concepts.png)
 <br/><br/>

 ## **Docker containers**:
 **Docker Container is a standardized unit which can be created on the fly to deploy a particular application or environment.**
 <br/><br/>

 ## **Docker images**:
 **Docker Image can be compared to a template which is used to create Docker Containers.They are the building blocks of a Docker Container. These Docker Images are created using the build command.**
 <br/><br/>

 ## **Docker registory**:
 **Docker Registry is where the Docker Images are stored. The Registry can be either a user’s local repository or a public repository like a Docker Hub allowing multiple users to collaborate in building an application.**
 <br/><br/>

 # **Basic docker commands**
 <br/>
 ![commands](https://raw.githubusercontent.com/sangam14/dockercheatsheets/master/dockercheatsheet7.png)

